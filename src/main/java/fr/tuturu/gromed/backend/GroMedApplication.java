package fr.tuturu.gromed.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class GroMedApplication {

	public static void main(String[] args) {
		SpringApplication.run(GroMedApplication.class, args);
	}
}
