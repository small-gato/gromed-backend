package fr.tuturu.gromed.backend.auth.controller;

import fr.tuturu.gromed.backend.auth.data.Tokens;
import fr.tuturu.gromed.backend.auth.dto.RefreshTokenDTO;
import fr.tuturu.gromed.backend.auth.exception.BadRefreshTokenException;
import fr.tuturu.gromed.backend.auth.service.JwtTokenService;
import fr.tuturu.gromed.backend.auth.service.RefreshTokenService;
import fr.tuturu.gromed.backend.auth.service.UserService;
import fr.tuturu.gromed.backend.common.model.User;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
@Tag(name = "Jwt Token Controller",description = "Ce controller permet la gestion des token a là fois pour en optenir un ou pour en renouveler un")
public class JwtTokenController {

    @Autowired
    JwtEncoder encoder;

    @Autowired
    JwtTokenService jwtTokenService;

    @Autowired
    RefreshTokenService refreshTokenService;

    @Autowired
    UserService userService;

    /**
     * @param authentication the authenticated user
     * @return JWT Token
     */
    @PostMapping(value = "/token", produces = MediaType.APPLICATION_JSON_VALUE)
    @SecurityRequirement(name = "basicAuth")
    public Tokens getTokens(Authentication authentication) {
        String jwtToken = jwtTokenService.createTokenFromAuthentication(authentication);
        String refreshToken = refreshTokenService.createRefreshToken(authentication.getName()).getToken();
        return new Tokens(jwtToken, refreshToken);
    }

    @PostMapping(value = "/refreshtoken", produces = MediaType.APPLICATION_JSON_VALUE)
    public Tokens refreshTokens(@RequestBody RefreshTokenDTO refreshTokenDTO) throws BadRefreshTokenException {
            return refreshTokenService.getNewTokens(refreshTokenDTO);
    }

    @GetMapping(value = "/me")
    @SecurityRequirement(name = "bearerAuth")
    public User getUser(Authentication authentication){
        return userService.getUserFromAuthentification(authentication);
    }
}
