package fr.tuturu.gromed.backend.auth.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
@SecurityRequirement(name = "bearerAuth")
@Tag(name = "Hello Controller", description = "Se controller de test permet de verifier que l'autentification via token jwt et la verification des roles fonctionne correctement")
public class HelloController {

    /**
     * @param authentication authentication user
     * @return String simple text to verify if it works
     */
    @GetMapping("/hello")
    @Operation(description = "Permet de verifier si un l'authentification fonctionne")
    public String hello(Authentication authentication) {
        return "Hello, " + authentication.getName() + "!";
    }

    /**
     * @param authentication authentication user
     * @return String simple text to verify if it works
     * <p>
     * The @PreAuthorize annotation is used to check for authorization before executing the method.
     * In this case, the method will only be executed if the current user has the 'SCOPE_ADMIN' authority.
     * The authority corresponding to a role assign to a user is : 'SCOPE_' + role.name
     */
    @GetMapping("/admin")
    @PreAuthorize("hasAuthority('SCOPE_ADMIN')")
    @Operation(description = "Permet de verifier que l'utilisateur dispose du role admin et le cas echéant lui affiche la route")
    public String admin(Authentication authentication) {
        return "Hello, " + authentication.getName() + " -- Admin";
    }
}
