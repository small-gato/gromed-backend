package fr.tuturu.gromed.backend.auth.exception;

import fr.tuturu.gromed.backend.common.exception.BadRequestException;
import org.springframework.http.HttpStatus;

public class EmptyRoleExceptions extends BadRequestException {

    public EmptyRoleExceptions(){
        super(HttpStatus.BAD_REQUEST.getReasonPhrase(),"The default role is not inside the database");
    }

}
