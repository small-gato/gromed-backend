package fr.tuturu.gromed.backend.auth.service;

import fr.tuturu.gromed.backend.auth.dto.UserDTO;
import fr.tuturu.gromed.backend.auth.exception.EmptyRoleExceptions;
import fr.tuturu.gromed.backend.auth.exception.ExistingUserException;
import fr.tuturu.gromed.backend.auth.model.Role;
import fr.tuturu.gromed.backend.common.model.User;
import fr.tuturu.gromed.backend.auth.repository.RoleRepository;
import fr.tuturu.gromed.backend.auth.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
public class UserService {

    private static final String DEFAULT_ROLE = "USER";

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    public User register(UserDTO userDTO) throws ExistingUserException, EmptyRoleExceptions {
        //Create password encoder using the delegatingPasswordEncoder
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        //Encode the password
        String encodedPassword = encoder.encode(userDTO.getPassword());

        //Create user with default role
        Optional<Role> role = roleRepository.findById(DEFAULT_ROLE);
        if(role.isEmpty()) throw new EmptyRoleExceptions();
        User user = new User(userDTO.getEmail(), encodedPassword,userDTO.getFirstName(),userDTO.getLastName(), Set.of(role.get()));


        //check if user is already existing
        if (userRepository.existsById(userDTO.getEmail())) {
            throw new ExistingUserException();
        } else
            //save user in database
            userRepository.save(user);
        return user;
    }

    public User getUserFromAuthentification(Authentication authentication){
        return  this.userRepository.findByEmail(authentication.getName());
    }

}
