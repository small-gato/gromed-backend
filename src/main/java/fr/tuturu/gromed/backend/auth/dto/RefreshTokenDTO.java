package fr.tuturu.gromed.backend.auth.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RefreshTokenDTO {

    private String refreshToken;
}
