package fr.tuturu.gromed.backend.auth.repository;


import fr.tuturu.gromed.backend.auth.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, String> {
}
