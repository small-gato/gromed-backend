package fr.tuturu.gromed.backend.auth.controller;

import fr.tuturu.gromed.backend.auth.data.Tokens;
import fr.tuturu.gromed.backend.auth.dto.UserDTO;
import fr.tuturu.gromed.backend.auth.exception.EmptyRoleExceptions;
import fr.tuturu.gromed.backend.auth.exception.ExistingUserException;
import fr.tuturu.gromed.backend.auth.service.JwtTokenService;
import fr.tuturu.gromed.backend.auth.service.RefreshTokenService;
import fr.tuturu.gromed.backend.auth.service.UserService;
import fr.tuturu.gromed.backend.common.model.User;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
@Tag(name = "Authentication Controller", description = "Ce controller permet la création de compte utilisateur")
public class AuthenticationController {

    @Autowired
    private UserService userService;

    @Autowired
    private JwtTokenService jwtTokenService;

    @Autowired
    private RefreshTokenService refreshTokenService;

    /**
     * @param userDTO DTO of the user
     * @return Tokens of the users
     */
    @PostMapping(value = "/register", produces = MediaType.APPLICATION_JSON_VALUE)
    @Transactional
    public Tokens register(@RequestBody UserDTO userDTO) throws ExistingUserException, EmptyRoleExceptions {
        //save and returns the new user
        User newUser = userService.register(userDTO);
        //create the tokens
        String jwtToken = jwtTokenService.createTokenFromUser(newUser);
        String refreshToken = refreshTokenService.createRefreshToken(newUser.getEmail()).getToken();
        return new Tokens(jwtToken, refreshToken);
    }


}
