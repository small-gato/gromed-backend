package fr.tuturu.gromed.backend.auth.exception;

import fr.tuturu.gromed.backend.common.exception.BadRequestException;
import org.springframework.http.HttpStatus;

public class ExistingUserException extends BadRequestException {

    public ExistingUserException() {
        super(HttpStatus.BAD_REQUEST.getReasonPhrase(),"This email address is already used");
    }
}
