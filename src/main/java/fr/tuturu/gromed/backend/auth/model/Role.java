package fr.tuturu.gromed.backend.auth.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Getter;

@Entity(name = "ROLE")
@Getter
public class Role {

    @Id
    private String name;

    @Column
    private String libelle;
}
