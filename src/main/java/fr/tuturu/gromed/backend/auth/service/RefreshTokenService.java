package fr.tuturu.gromed.backend.auth.service;

import fr.tuturu.gromed.backend.auth.data.Tokens;
import fr.tuturu.gromed.backend.auth.dto.RefreshTokenDTO;
import fr.tuturu.gromed.backend.auth.exception.BadRefreshTokenException;
import fr.tuturu.gromed.backend.auth.exception.RefreshTokenExpiredException;
import fr.tuturu.gromed.backend.auth.model.RefreshToken;
import fr.tuturu.gromed.backend.common.model.User;
import fr.tuturu.gromed.backend.auth.repository.RefreshTokenRepository;
import fr.tuturu.gromed.backend.auth.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Date;
import java.util.UUID;

@Service
public class RefreshTokenService {

    @Value("${refresh-token.expiry}")
    Long refreshTokenExpiry;

    @Autowired
    private RefreshTokenRepository refreshTokenRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtTokenService jwtTokenService;


    public RefreshToken findByToken(String token) throws BadRefreshTokenException {
        RefreshToken refreshToken = refreshTokenRepository.findByToken(token);
        if (refreshToken == null)
            throw new BadRefreshTokenException();
        return refreshToken;
    }


    public RefreshToken createRefreshToken(String userName) {
        User user = userRepository.findByEmail(userName);
        RefreshToken refreshToken = new RefreshToken();

        refreshToken.setUser(user);
        refreshToken.setExpiryDate(Date.from(Instant.now().plusSeconds(refreshTokenExpiry)));
        refreshToken.setToken(UUID.randomUUID().toString());

        refreshToken = refreshTokenRepository.save(refreshToken);
        return refreshToken;
    }

    public Tokens getNewTokens(RefreshTokenDTO refreshTokenDTO) throws BadRefreshTokenException {
        RefreshToken oldRefreshToken = findByToken(refreshTokenDTO.getRefreshToken());
        RefreshToken newRefreshToken = createRefreshToken(oldRefreshToken.getUser().getEmail());
        String newJwtToken = jwtTokenService.createTokenFromUser(oldRefreshToken.getUser());
        return new Tokens(newJwtToken, newRefreshToken.getToken());

    }

    public void verifyExpiration(RefreshToken token) throws RefreshTokenExpiredException {
        if (token.getExpiryDate().compareTo(Date.from(Instant.now())) < 0) {
            refreshTokenRepository.delete(token);
            throw new RefreshTokenExpiredException();
        }
    }
}
