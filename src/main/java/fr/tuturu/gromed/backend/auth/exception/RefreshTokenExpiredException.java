package fr.tuturu.gromed.backend.auth.exception;

import fr.tuturu.gromed.backend.common.exception.BadRequestException;
import org.springframework.http.HttpStatus;

public class RefreshTokenExpiredException extends BadRequestException {

    public RefreshTokenExpiredException() {
        super(HttpStatus.BAD_REQUEST.getReasonPhrase(),"Refresh token was expired");
    }
}
