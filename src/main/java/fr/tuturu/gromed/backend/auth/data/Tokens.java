package fr.tuturu.gromed.backend.auth.data;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Tokens {

    private static final String TOKEN_TYPE = "Bearer";
    private final String jwtToken;
    private final String refreshToken;

    public Tokens(String jwtToken, String refreshToken) {
        this.jwtToken = jwtToken;
        this.refreshToken = refreshToken;
    }
}
