package fr.tuturu.gromed.backend.auth.exception;

import fr.tuturu.gromed.backend.common.exception.BadRequestException;
import org.springframework.http.HttpStatus;

public class BadRefreshTokenException extends BadRequestException {

    public BadRefreshTokenException() {
        super(HttpStatus.BAD_REQUEST.getReasonPhrase(),"Invalid refresh token");
    }
}
