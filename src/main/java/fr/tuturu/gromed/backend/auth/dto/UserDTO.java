package fr.tuturu.gromed.backend.auth.dto;

import lombok.Getter;

@Getter
public class UserDTO {

    private final String email;
    private final String password;

    private final String firstName;

    private final String lastName;

    public UserDTO(String email, String password, String firstName, String lastName) {
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
    }
}
