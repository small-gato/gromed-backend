package fr.tuturu.gromed.backend.auth.repository;

import fr.tuturu.gromed.backend.common.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

    User findByEmail(String email);
}
