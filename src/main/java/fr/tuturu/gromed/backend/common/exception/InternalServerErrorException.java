package fr.tuturu.gromed.backend.common.exception;

public class InternalServerErrorException extends CustomException{
    public InternalServerErrorException(String code, String message) {
        super(code, message);
    }
}
