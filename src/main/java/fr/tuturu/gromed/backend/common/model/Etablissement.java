package fr.tuturu.gromed.backend.common.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Etablissement {

    @Id
    @Column(name = "nofiness_et", nullable = false)
    private String nofiness_et;

    @Column(name = "nofiness_ej", nullable = false)
    private String nofiness_ej;

    @Column(name = "code_postal")
    private String codePostal;

    @Column(name = "ville")
    private String ville;

    @ManyToOne
    @JoinColumn(name = "categorie_etablissement_id")
    private CategorieEtablissement categorieEtablissement;
}
