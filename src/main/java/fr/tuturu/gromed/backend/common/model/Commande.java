package fr.tuturu.gromed.backend.common.model;

import fr.tuturu.gromed.backend.common.exception.BadRequestException;
import fr.tuturu.gromed.backend.common.model.keys.ProduitCommandeKey;
import jakarta.persistence.*;
import jakarta.transaction.Transactional;
import lombok.*;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.Set;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Commande {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "identifiant_commande", nullable = false)
    private Long identifiantCommande;

    @ManyToOne
    @JoinColumn(name = "email")
    private User user;

    @Enumerated
    @Column(name = "statut")
    private CommandeStatut commandeStatut;

    @Column(name = "date_validation")
    private LocalDateTime dateValidation;

    @Column(name = "nom_commande_type")
    private String commandeTypeNom;

    @OneToMany(mappedBy = "commande", orphanRemoval = true)
    private Set<ProduitCommande> produitCommandes;

    public void setDateValidation(LocalDateTime dateValidation) {
        if (this.dateValidation == null) {
            this.dateValidation = dateValidation;
        }
    }

    @Transactional
    public ProduitCommande addPresentation(Presentation presentation, int quantite) {
        try {
            return editQuantite(presentation, quantite);
        } catch (BadRequestException bre) {
            ProduitCommande produitCommande = ProduitCommande.builder()
                    .key(new ProduitCommandeKey(presentation.getCodeCip(), this.identifiantCommande))
                    .quantite(quantite)
                    .commande(this)
                    .presentation(presentation)
                    .build();
            produitCommandes.add(produitCommande);
            return produitCommande;
        }
    }

    public ProduitCommande editQuantite(Presentation presentation, int newQuantite) throws BadRequestException {
        ProduitCommande produitCommande = searchProduitCommande(presentation);
        produitCommande.editQuantite(newQuantite);
        return produitCommande;
    }

    public ProduitCommande replaceQuantite(Presentation presentation, int newQuantite) throws BadRequestException {
        ProduitCommande produitCommande = searchProduitCommande(presentation);
        produitCommande.replaceQuantite(newQuantite);
        return produitCommande;
    }

    public ProduitCommande searchProduitCommande(Presentation presentation) throws BadRequestException {
        Optional<ProduitCommande> optProduitCommande = produitCommandes.stream()
                .filter(pc -> pc.getKey().getPresentationId()
                        .equals(presentation.getCodeCip())
                ).findFirst();
        if (optProduitCommande.isEmpty()) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST.getReasonPhrase(), "La présentation n'existe pas dans cette commande");
        }
        return optProduitCommande.get();
    }

    public void deletePresentation(Presentation presentation) throws BadRequestException {
        if (!produitCommandes.remove(
                ProduitCommande.builder()
                        .key(new ProduitCommandeKey(presentation.getCodeCip(),this.identifiantCommande))
                        .quantite(1)
                        .build()
        )
        ) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST.getReasonPhrase(), "La présentation n'existe pas dans cette commande");
        }
    }

    public boolean isEmpty() {
        return this.produitCommandes.isEmpty();
    }
}
