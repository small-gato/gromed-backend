package fr.tuturu.gromed.backend.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.tuturu.gromed.backend.auth.model.Role;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.LinkedHashSet;
import java.util.Set;

@Entity(name = "users")
@Getter
public class User {

    @Id
    @Column(nullable = false)
    private String email;

    private String firstName;

    private String lastName;

    @JsonIgnore
    @Column(nullable = false)
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "users_roles",
            joinColumns = @JoinColumn(name = "email"),
            inverseJoinColumns = @JoinColumn(name = "roles_name"))
    private Set<Role> roles = new LinkedHashSet<>();

    @Setter
    @ManyToOne
    @JoinColumn(name = "nofiness_et")
    private Etablissement etablissement;

    public User(String email, String password, String firstName, String lastName, Set<Role> roles) {
        this.email = email;
        this.password = password;
        this.roles = roles;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public User() {}
}
