package fr.tuturu.gromed.backend.common.model.keys;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class MedicamentGroupeKey implements Serializable {

    private Long medicamentId;

    private Long groupeId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MedicamentGroupeKey that)) return false;
        return medicamentId.equals(that.medicamentId) && groupeId.equals(that.groupeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(medicamentId, groupeId);
    }
}
