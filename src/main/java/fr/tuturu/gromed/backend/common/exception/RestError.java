package fr.tuturu.gromed.backend.common.exception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class RestError {
    private Object body;
    private String code;
    private Integer httpStatus;
    private String exceptionName;
    private String exceptionMessage;
    private String path;

    public RestError(Object body, String code, Integer httpStatus, Exception exception, String path) {
        this.body = body;
        this.code = code;
        this.httpStatus = httpStatus;
        this.exceptionName = exception.getClass().toString();
        this.exceptionMessage = exception.getMessage();
        this.path = path;
    }

    @Override
    public String toString() {
        return "RestError{" +
                "body=" + body +
                ", httpStatus=" + httpStatus +
                ", exceptionName='" + exceptionName + '\'' +
                ", exceptionMessage='" + exceptionMessage + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
