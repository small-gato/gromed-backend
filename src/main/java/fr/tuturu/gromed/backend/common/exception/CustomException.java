package fr.tuturu.gromed.backend.common.exception;

public abstract class CustomException extends Exception {

    private final ErreurInfo erreur;

    protected CustomException(String code, String message) {
        super(message);
        this.erreur = new ErreurInfo(code, message) {
        };
    }


    public ErreurInfo getErreurInfo() {
        return this.erreur;
    }
}
