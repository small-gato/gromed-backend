package fr.tuturu.gromed.backend.common.repository;

import fr.tuturu.gromed.backend.common.model.Substance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface SubstanceRepository extends JpaRepository<Substance, Long> {
    @Transactional
    @Query("select s from Substance s where s.codeSubstance=:substanceCode")
    Optional<Substance> findById(@Param("substanceCode") Long substanceCode);
}
