package fr.tuturu.gromed.backend.common.exception;

import java.io.Serializable;

public abstract class ErreurInfo implements Serializable {

    private final String message;

    private final String code;

    protected ErreurInfo(String code, String message) {
        this.message = message;
        this.code = code;
    }

    public String getMessage() {
        return this.message;
    }

    public String getCode() {
        return this.code;
    }

    @Override
    public String toString() {
        return "ErreurInfo{" +
                "message=" + message +
                ", code=" + code +
                '}';
    }
}
