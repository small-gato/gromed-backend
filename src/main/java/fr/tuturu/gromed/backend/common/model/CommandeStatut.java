package fr.tuturu.gromed.backend.common.model;

public enum CommandeStatut {
    EN_ATTENTE,
    EN_COURS,
    VALIDEE,
    EN_PREPARATION,
    EN_LIVRAISON,
    ANNULEE,
}
