package fr.tuturu.gromed.backend.common.repository;

import fr.tuturu.gromed.backend.common.model.Commande;
import fr.tuturu.gromed.backend.common.model.CommandeStatut;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Repository
public interface CommandeRepository extends JpaRepository<Commande, Long> {

    Optional<Commande> findByIdentifiantCommandeAndUser_EmailAndCommandeStatut(Long identifiantCommande, String email, CommandeStatut commandeStatut);

    Optional<Commande> findByUser_EmailAndCommandeStatut(String email, CommandeStatut CommandeStatut);

    List<Commande> findByUser_EmailAndCommandeStatutIn(String email, Collection<CommandeStatut> commandeStatuts);

    Optional<Commande> findByUser_EmailAndIdentifiantCommande(String email, Long identifiantCommande);

    boolean existsByUser_EmailAndIdentifiantCommande(String email, Long identifiantCommande);
    Optional<Commande> findByIdentifiantCommande(Long identifiantCommande);
}
