package fr.tuturu.gromed.backend.common.controller;

import fr.tuturu.gromed.backend.common.exception.BadRequestException;
import fr.tuturu.gromed.backend.common.model.Presentation;
import fr.tuturu.gromed.backend.common.service.PresentationService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@SecurityRequirement(name = "bearerAuth")
@Tag(name = "Presentation controller", description = "Controller des presentations")
@RequestMapping(value= "/presentations", produces = MediaType.APPLICATION_JSON_VALUE)
public class PresentationRestController {

    @Autowired
    private PresentationService presentationService;

    @GetMapping(path = "/presentation/{codeCip}")
    public Presentation getPresentation(@PathVariable Long codeCip) throws BadRequestException {
        return presentationService.getPresentation(codeCip);
    }

    @GetMapping("/all")
    public Page<Presentation> getPresentations(@RequestParam (defaultValue = "0") int page, @RequestParam (defaultValue = "10") int size) {
        Pageable paging = PageRequest.of(page, size);
        return presentationService.getPresentations(paging);
    }

    @GetMapping("/search")
    public Page<Presentation> searchPresentation(
            @RequestParam (defaultValue = "0") int page,
            @RequestParam (defaultValue = "10") int size,
            @RequestParam(name = "sort", defaultValue = "codeCip") String sortBy,
            @RequestParam(name = "direction", defaultValue = "ASC" )String direction,
            @RequestParam(name = "fabricants",required = false) List<String> fabricants,
            @RequestParam(name = "formesmed",required = false) List<String> formesMed,
            @RequestParam(name = "typegens",required = false) List<Integer> typegens,
            @RequestParam(name = "substances", required = false) List<String> substances,
            @RequestParam(name = "medicaments", required = false) List<String> medicaments){

        Pageable paging = PageRequest.of(page, size, Sort.Direction.valueOf(direction),sortBy);
        return presentationService.searchPresentation(formesMed,fabricants,typegens,substances, medicaments, paging);
    }

    @GetMapping("/autocomplete/fabricant")
    public List<String> getFabricants(@RequestParam(name = "term") String term){
        return presentationService.getFabricants(term);
    }

    @GetMapping("/autocomplete/formemed")
    public List<String> getFormeMed(@RequestParam(name = "term") String term){
        return presentationService.getFormeMed(term);
    }

    @GetMapping("/autocomplete/substance")
    public List<String> getSubstance(@RequestParam(name = "term") String term){
        return presentationService.getSubstance(term);
    }

    @GetMapping("/autocomplete/medicament")
    public List<String> getMedicament(@RequestParam(name = "term") String term){
        return presentationService.getMedicament(term);
    }

    @GetMapping("/presentation/substances/{codeCis}")
    public List<String> getSubstances(@PathVariable Long codeCis) {
        return presentationService.getSubstances(codeCis);
    }

    @GetMapping("/presentation/prescriptions/{codeCis}")
    public List<String> getPrescriptions(@PathVariable Long codeCis) {
        return presentationService.getPrescriptions(codeCis);
    }

    @GetMapping("/group/{codeCis}")
    public List<Presentation> getPresentationsSameGroup(@PathVariable Long codeCis) {
        return presentationService.getPresentationsSameGroup(codeCis);
    }
}
