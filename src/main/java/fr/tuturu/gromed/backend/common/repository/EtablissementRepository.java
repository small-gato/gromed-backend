package fr.tuturu.gromed.backend.common.repository;

import fr.tuturu.gromed.backend.common.model.Etablissement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface EtablissementRepository extends JpaRepository<Etablissement, Long> {
    @Transactional
    @Query("select e from Etablissement e where e.nofiness_et=:noFinessEt")
    Optional<Etablissement> findById(@Param("noFinessEt") Long noFinesset);
}
