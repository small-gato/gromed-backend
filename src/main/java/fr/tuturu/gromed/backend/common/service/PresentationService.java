package fr.tuturu.gromed.backend.common.service;

import fr.tuturu.gromed.backend.common.exception.BadRequestException;
import fr.tuturu.gromed.backend.common.model.Presentation;
import fr.tuturu.gromed.backend.common.repository.MedicamentAppartientGroupeGenRepository;
import fr.tuturu.gromed.backend.common.repository.MedicamentContientSubstanceRepository;
import fr.tuturu.gromed.backend.common.repository.MedicamentRepository;
import fr.tuturu.gromed.backend.common.repository.PresentationRepository;
import fr.tuturu.gromed.backend.common.specification.PresentationSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PresentationService {

    @Autowired
    private PresentationRepository presentationRepository;
    @Autowired
    private MedicamentContientSubstanceRepository medicamentContientSubstanceRepository;
    @Autowired
    private MedicamentRepository medicamentRepository;
    @Autowired
    private MedicamentAppartientGroupeGenRepository medicamentAppartientGroupeGenRepository;

    public Presentation getPresentation(Long codeCip) throws BadRequestException {
        Optional<Presentation> presentationOptional = presentationRepository.findByCodeCip(codeCip);
        if (presentationOptional.isEmpty()) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST.getReasonPhrase(),"Le code CIP n'existe pas.");
        }
        return presentationOptional.get();
    }

    public Page<Presentation> getPresentations(Pageable p) {
        return presentationRepository.findAllPageable(p);
    }

    public Page<Presentation> searchPresentation(List<String> formesMed, List<String> fabricants, List<Integer> typeGens, List<String> substances, List<String> medicaments, Pageable page){
        return this.presentationRepository.findAll(new PresentationSpecification(formesMed,fabricants,typeGens,substances, medicaments),page);
    }

    public List<String> getFabricants(String term) {
        return presentationRepository.findFabricants(term.toLowerCase());
    }

    public List<String> getFormeMed(String term) {
        return presentationRepository.findFormeMed(term.toLowerCase());
    }

    public List<String> getSubstance(String term) {
        return presentationRepository.findSubstance(term.toLowerCase());
    }

    public List<String> getSubstances(Long codeCis) {
        return medicamentContientSubstanceRepository.findSubstancesByMedicament(codeCis);
    }

    public List<String> getPrescriptions(Long codeCis) {
        return medicamentRepository.getPrescriptions(codeCis);
    }

    public List<Presentation> getPresentationsSameGroup(Long codeCis) {
        return medicamentAppartientGroupeGenRepository.findPresentationsByCodeCisGroup(codeCis);
    }

    public List<String> getMedicament(String term) {
        return presentationRepository.findNomMedicament(term);
    }
}
