package fr.tuturu.gromed.backend.common.service;

import fr.tuturu.gromed.backend.auth.repository.UserRepository;
import fr.tuturu.gromed.backend.common.exception.BadRequestException;
import fr.tuturu.gromed.backend.common.model.Commande;
import fr.tuturu.gromed.backend.common.model.CommandeStatut;
import fr.tuturu.gromed.backend.common.model.Presentation;
import fr.tuturu.gromed.backend.common.model.ProduitCommande;
import fr.tuturu.gromed.backend.common.repository.CommandeRepository;
import fr.tuturu.gromed.backend.common.repository.PresentationRepository;
import fr.tuturu.gromed.backend.common.repository.ProduitCommandeRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
@Transactional
public class CommandeService {

    @Autowired
    private CommandeRepository commandeRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProduitCommandeRepository produitCommandeRepository;

    @Autowired
    private PresentationRepository presentationRepository;

    @Transactional
    public Commande getPanier(Authentication authentication) {
        Optional<Commande> commandeOptional = commandeRepository.findByUser_EmailAndCommandeStatut(authentication.getName(), CommandeStatut.EN_COURS);
        if (commandeOptional.isEmpty()) {
            Set<ProduitCommande> produitCommandes = new LinkedHashSet<>();
            Commande newCommande = Commande.builder().commandeStatut(CommandeStatut.EN_COURS).user(userRepository.findByEmail(authentication.getName())).produitCommandes(produitCommandes).build();
            commandeRepository.save(newCommande);
            return newCommande;
        }
        return commandeOptional.get();
    }

    @Transactional
    public Commande addPresentation(Commande panier, Presentation presentation, int quantite) {
        ProduitCommande produitCommande = panier.addPresentation(presentation, quantite);
        produitCommandeRepository.save(produitCommande);
        return panier;
    }

    @Transactional
    public Commande validerPanier(Commande panier) throws BadRequestException {
        if (panier.isEmpty()) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST.getReasonPhrase(),"Le panier est vide.");
        }
        panier.setCommandeStatut(CommandeStatut.VALIDEE);
        panier.setDateValidation(LocalDateTime.now());
        decreaseStockLogique(panier.getProduitCommandes());
        commandeRepository.save(panier);
        ScheduledExecutorService ex = Executors.newSingleThreadScheduledExecutor();
        ex.schedule(() -> setStatutEnPreparation(panier.getIdentifiantCommande()), 30, TimeUnit.MINUTES);
        return panier;
    }

    @Transactional
    public Commande editQuantity(Commande panier, Presentation presentation, int newQuantity) throws BadRequestException {
        ProduitCommande produitCommande = panier.replaceQuantite(presentation, newQuantity);
        produitCommandeRepository.save(produitCommande);
        return panier;
    }

    public List<Commande> getHistoriqueCommande(String name) {
        return this.commandeRepository.findByUser_EmailAndCommandeStatutIn(name, List.of(CommandeStatut.VALIDEE,CommandeStatut.EN_PREPARATION,CommandeStatut.EN_LIVRAISON));
    }

    public Commande getSpecificCommand(String name, long id) throws BadRequestException {
        Optional<Commande> commandeOptional = this.commandeRepository.findByUser_EmailAndIdentifiantCommande(name,id);
        if (commandeOptional.isEmpty()) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST.getReasonPhrase(),"La commande n'existe pas pour se compte");
        }
        return commandeOptional.get();
    }

    public boolean checkIfCommandExist(String email, long id){
        return this.commandeRepository.existsByUser_EmailAndIdentifiantCommande(email,id);
    }

    @Transactional
    public Commande getCommandeAnnulable(final Long identifiantCommande, Authentication authentication) throws BadRequestException {
        Optional<Commande> optCommande = commandeRepository.findByIdentifiantCommandeAndUser_EmailAndCommandeStatut(identifiantCommande, authentication.getName(), CommandeStatut.VALIDEE);
        if (optCommande.isEmpty()) {
            throw new BadRequestException(HttpStatus.BAD_REQUEST.getReasonPhrase(),"La commande n'est pas annulable.");
        }
        Commande commandeAnnulee = optCommande.get();
        commandeAnnulee.setCommandeStatut(CommandeStatut.ANNULEE);
        increaseStockLogique(commandeAnnulee.getProduitCommandes());
        commandeRepository.save(commandeAnnulee);
        return commandeAnnulee;
    }

    @Transactional
    public void setStatutEnPreparation(Long identifiantCommande) {
        Optional<Commande> optCommande = commandeRepository.findByIdentifiantCommande(identifiantCommande);
        if (optCommande.isPresent()) {
            Commande commande = optCommande.get();
            if (commande.getCommandeStatut() == CommandeStatut.VALIDEE) {
                commande.setCommandeStatut(CommandeStatut.EN_PREPARATION);
                commandeRepository.save(commande);
            }
        }
    }

    @Transactional
    public void decreaseStockLogique(Set<ProduitCommande> produitCommandes) {
        for (ProduitCommande produitCommande:
             produitCommandes) {
            Presentation presentation = produitCommande.getPresentation();
            presentation.deduireStockLogique(produitCommande.getQuantite());
            presentationRepository.save(presentation);
        }
    }

    @Transactional
    public void increaseStockLogique(Set<ProduitCommande> produitCommandes) {
        for (ProduitCommande produitCommande:
                produitCommandes) {
            Presentation presentation = produitCommande.getPresentation();
            if (presentation.getStockLogique()<1){
                presentation.ajouterStockLogique(Math.toIntExact(presentation.getStockPhysique()));
            }else{
                presentation.ajouterStockLogique(produitCommande.getQuantite());
            }
            presentationRepository.save(presentation);
        }
    }
}
