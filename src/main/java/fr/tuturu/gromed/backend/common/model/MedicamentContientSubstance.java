package fr.tuturu.gromed.backend.common.model;


import fr.tuturu.gromed.backend.common.model.keys.SubstanceMedicamentKey;
import jakarta.persistence.*;

@Entity
public class MedicamentContientSubstance {

    @EmbeddedId
    private SubstanceMedicamentKey key;

    @ManyToOne
    @MapsId("substanceId")
    @JoinColumn(name = "code_substance")
    private Substance substance;

    @ManyToOne
    @MapsId("medicamentId")
    @JoinColumn(name = "code_cis")
    private Medicament medicament;

    @Column(name = "dosage")
    private String dosage;

}
