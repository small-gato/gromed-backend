package fr.tuturu.gromed.backend.common.repository;

import fr.tuturu.gromed.backend.common.model.Medicament;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface MedicamentRepository extends JpaRepository<Medicament, Long> {
    @Transactional
    @Query("select m from Medicament m where m.codeCIS=:cisCode")
    Optional<Medicament> findById(@Param("cisCode") Long cisCode);

    @Query("select p.nomPrescription From Medicament m join m.prescriptions p where m.codeCIS=:cisCode")
    List<String> getPrescriptions(@Param("cisCode") Long cisCode);
}
