package fr.tuturu.gromed.backend.common.controller;

import fr.tuturu.gromed.backend.common.exception.BadRequestException;
import fr.tuturu.gromed.backend.common.model.Commande;
import fr.tuturu.gromed.backend.common.model.Presentation;
import fr.tuturu.gromed.backend.common.service.CommandeService;
import fr.tuturu.gromed.backend.common.service.PresentationService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@SecurityRequirement(name = "bearerAuth")
@Tag(name = "Command controller", description = "Controller des commandes")
@RequestMapping(value= "/commandes", produces = MediaType.APPLICATION_JSON_VALUE)
public class CommandeRestController {

    @Autowired
    private CommandeService commandeService;

    @Autowired
    private PresentationService presentationService;

    @GetMapping(path = "/panier")
    public Commande getPanier(Authentication authentication) {
        return commandeService.getPanier(authentication);
    }

    @PutMapping(path = "/panier/add/{codeCip}")
    public Commande addPresentation(@PathVariable("codeCip") final long codeCip, @RequestParam final int quantite, Authentication authentication) {
        Commande panierToUpdate = commandeService.getPanier(authentication);
        try {
            Presentation presentation = presentationService.getPresentation(codeCip);
            return commandeService.addPresentation(panierToUpdate, presentation, quantite);
        } catch (BadRequestException bre) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "presentation not found"
            );
        }
    }

    @PutMapping(path = "/panier/validate")
    public Commande validerPanier(Authentication authentication) {
        try {
            return commandeService.validerPanier(commandeService.getPanier(authentication));
        } catch (BadRequestException bre) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_ACCEPTABLE, "panier vide"
            );
        }
    }

    @PutMapping(path = "/panier/edit/{codeCip}")
    public Commande editPresentation(@PathVariable("codeCip") final Long codeCip, @RequestParam final int quantite, Authentication authentication) {
        try {
            Presentation presentation = presentationService.getPresentation(codeCip);
            try {
                return commandeService.editQuantity(commandeService.getPanier(authentication), presentation, quantite);
            } catch (BadRequestException bre) {
                throw new ResponseStatusException(
                        HttpStatus.NOT_FOUND, bre.getMessage()
                );
            }
        } catch (BadRequestException bre) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, bre.getMessage()
            );
        }
    }
    @PutMapping(path = "/commandes/annuler/{identifiantCommande}")
    public Commande annulerCommande(@PathVariable("identifiantCommande") final Long identifiantCommande, Authentication authentication) {
        try {
            return commandeService.getCommandeAnnulable(identifiantCommande, authentication);
        } catch (BadRequestException bre) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, bre.getMessage()
            );
        }
    }

    @GetMapping("/history")
    public List<Commande> getHistoriqueCommand(Authentication authentication) {
        return this.commandeService.getHistoriqueCommande(authentication.getName());
    }

    @GetMapping(path = "/history/{id}")
    public Commande getCommand(Authentication authentication, @PathVariable long id) throws BadRequestException {
        return this.commandeService.getSpecificCommand(authentication.getName(), id);
    }

    @RequestMapping(path = "/history/{id}", method = {RequestMethod.HEAD})
    public ResponseEntity<Void> checkIfCommandExist(Authentication authentication, @PathVariable long id) {
        if (this.commandeService.checkIfCommandExist(authentication.getName(), id)) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.notFound().build();
    }
}