package fr.tuturu.gromed.backend.common.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Collection;

@Entity
@Getter
@Setter
public class Presentation {
    @Id
    @Column(name = "code_cip", nullable = false)
    private Long codeCip;

    @Column(name = "libelle_presentation")
    private String libellePresentation;

    @Column(name = "prix")
    private Double prix;

    @Setter(lombok.AccessLevel.NONE)
    @Column(name = "stock_physique")
    private Long stockPhysique;

    @Setter(lombok.AccessLevel.NONE)
    @Column(name = "stock_logique")
    private Long stockLogique;

    @Column(name = "active")
    private Boolean active;

    @ManyToMany
    private Collection<Medicament> medicaments;

    public void deduireStockLogique(int quantite) {
        if (quantite > stockLogique) {
            stockLogique = (long) 0;
        } else {
            stockLogique -= quantite;
        }
    }

    public void ajouterStockLogique(int quantite) {
        stockLogique += quantite;
    }
}
