package fr.tuturu.gromed.backend.common.repository;

import fr.tuturu.gromed.backend.common.model.Presentation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PresentationRepository extends JpaRepository<Presentation, Long>, JpaSpecificationExecutor<Presentation> {
    Optional<Presentation> findByCodeCip(Long codeCip);

    @Query("select p from Presentation p")
    Page<Presentation> findAllPageable(Pageable pageable);

    @Query("select distinct m.fabricant from Presentation p join p.medicaments m where lower(m.fabricant) like %:term%")
    List<String> findFabricants(String term);

    @Query("select distinct m.formePharmacetique from Presentation p join p.medicaments m where lower(m.formePharmacetique) like %:term%")
    List<String> findFormeMed(String term);

    @Query("select distinct s.substance.nomSubstance from Presentation p join p.medicaments m join m.substances s where lower(s.substance.nomSubstance) like %:term%")
    List<String> findSubstance(String term);

    @Query("select distinct m.nomMedicament from Presentation p join p.medicaments m where lower(m.nomMedicament) like %:term%")
    List<String> findNomMedicament(String term);
}
