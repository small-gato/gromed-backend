package fr.tuturu.gromed.backend.common.exception;

public class NotFoundException extends CustomException {

    public NotFoundException(String code, String message) {
        super(code, message);
    }

}
