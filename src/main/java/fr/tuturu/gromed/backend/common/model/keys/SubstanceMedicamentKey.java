package fr.tuturu.gromed.backend.common.model.keys;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;

import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class SubstanceMedicamentKey implements Serializable {

    private Long medicamentId;

    private Long substanceId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SubstanceMedicamentKey that)) return false;
        return medicamentId.equals(that.medicamentId) && substanceId.equals(that.substanceId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(medicamentId, substanceId);
    }
}
