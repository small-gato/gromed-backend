package fr.tuturu.gromed.backend.common.repository;

import fr.tuturu.gromed.backend.common.model.GroupeGenerique;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface GroupeGeneriqueRepository extends JpaRepository<GroupeGenerique, Long> {
    @Transactional
    @Query("select g from GroupeGenerique g where g.idGroupe=:groupeId")
    Optional<GroupeGenerique> findById(@Param("groupeId") Long groupeId);
}
