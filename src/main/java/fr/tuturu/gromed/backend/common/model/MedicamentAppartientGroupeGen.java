package fr.tuturu.gromed.backend.common.model;


import fr.tuturu.gromed.backend.common.model.keys.MedicamentGroupeKey;
import jakarta.persistence.*;

@Entity
public class MedicamentAppartientGroupeGen {

    @EmbeddedId
    private MedicamentGroupeKey key = new MedicamentGroupeKey();

    @ManyToOne
    @MapsId("medicamentId")
    @JoinColumn(name = "code_cis")
    private Medicament medicament;

    @ManyToOne
    @MapsId("groupeId")
    @JoinColumn(name = "id_groupe")
    private fr.tuturu.gromed.backend.common.model.GroupeGenerique groupe;

    @Column(name = "type_generique", nullable = false)
    private Integer typeGen;


}
