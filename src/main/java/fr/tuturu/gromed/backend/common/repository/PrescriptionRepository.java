package fr.tuturu.gromed.backend.common.repository;

import fr.tuturu.gromed.backend.common.model.Prescription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface PrescriptionRepository extends JpaRepository<Prescription, String> {
    @Transactional
    @Query("select p from Prescription p where p.nomPrescription=:prescriptionNom")
    Optional<Prescription> findByName(@Param("prescriptionNom") String prescriptionNom);
}
