package fr.tuturu.gromed.backend.common.model.keys;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ProduitCommandeKey implements Serializable {

    private Long presentationId;

    private Long commandeId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProduitCommandeKey that)) return false;
        return presentationId.equals(that.presentationId) && commandeId.equals(that.commandeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(presentationId, commandeId);
    }
}
