package fr.tuturu.gromed.backend.common.specification;

import fr.tuturu.gromed.backend.common.model.*;
import jakarta.persistence.criteria.*;
import lombok.AllArgsConstructor;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
public class PresentationSpecification implements Specification<Presentation> {
    private final List<String> formePharmacetiques;
    private final List<String> fabricants;
    private final List<Integer> typeGens;
    private final List<String> substances;
    private final List<String> medicaments;

    @Override
    public Predicate toPredicate(Root<Presentation> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
        List<Predicate> predicates = new ArrayList<>();
        Join<Presentation, Medicament> medicamentJoin = root.join("medicaments", JoinType.INNER);


        if (formePharmacetiques != null && !formePharmacetiques.isEmpty()) {
            predicates.add(medicamentJoin.get("formePharmacetique").in(formePharmacetiques));
        }

        if (fabricants != null && !fabricants.isEmpty()) {
            Predicate[] fabricantPredicates = fabricants.stream()
                    .map(fabricant -> criteriaBuilder.like(criteriaBuilder.lower(medicamentJoin.get("fabricant")), "%" + fabricant.toLowerCase() + "%"))
                    .toArray(Predicate[]::new);
            predicates.add(criteriaBuilder.or(fabricantPredicates));
        }

        if (typeGens != null && !typeGens.isEmpty()) {
            Join<Medicament, MedicamentAppartientGroupeGen> groupJoin = medicamentJoin.join("groupes", JoinType.LEFT);
            predicates.add(groupJoin.get("typeGen").in(typeGens));
        }

        if (substances != null && !substances.isEmpty()){
            Join<Medicament, MedicamentContientSubstance> substancePivotJoin = medicamentJoin.join("substances", JoinType.LEFT);
            Join<MedicamentContientSubstance, Substance> substanceJoin = substancePivotJoin.join("substance", JoinType.LEFT);
            Predicate[] substancePredicates = substances.stream()
                    .map(substance -> criteriaBuilder.like(criteriaBuilder.lower(substanceJoin.get("nomSubstance")), "%" + substance.toLowerCase() + "%"))
                    .toArray(Predicate[]::new);
            predicates.add(criteriaBuilder.or(substancePredicates));
        }

        if (medicaments != null && !medicaments.isEmpty()) {
            Predicate[] medicamentPredicates = medicaments.stream()
                    .map(medicament->criteriaBuilder.like(criteriaBuilder.lower(medicamentJoin.get("nomMedicament")),"%"+medicament.toLowerCase()+"%"))
                    .toArray(Predicate[]::new);
            predicates.add(criteriaBuilder.or(medicamentPredicates));
        }

        return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
    }
}
