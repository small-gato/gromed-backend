package fr.tuturu.gromed.backend.common.repository;

import fr.tuturu.gromed.backend.common.model.MedicamentContientSubstance;
import fr.tuturu.gromed.backend.common.model.keys.SubstanceMedicamentKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicamentContientSubstanceRepository extends JpaRepository<MedicamentContientSubstance, SubstanceMedicamentKey> {

    @Query("select m.substance.nomSubstance from MedicamentContientSubstance m where m.medicament.codeCIS=:cisCode")
    List<String> findSubstancesByMedicament(@Param("cisCode") Long cisCode);
}
