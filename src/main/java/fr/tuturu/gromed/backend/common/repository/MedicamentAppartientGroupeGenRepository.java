package fr.tuturu.gromed.backend.common.repository;

import fr.tuturu.gromed.backend.common.model.MedicamentAppartientGroupeGen;
import fr.tuturu.gromed.backend.common.model.Presentation;
import fr.tuturu.gromed.backend.common.model.keys.MedicamentGroupeKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MedicamentAppartientGroupeGenRepository extends JpaRepository<MedicamentAppartientGroupeGen, MedicamentGroupeKey> {
    @Query("select distinct p from Presentation p join p.medicaments m join m.groupes g where g.groupe.idGroupe in (select mg.groupe.idGroupe from MedicamentAppartientGroupeGen mg where mg.medicament.codeCIS=:cisCode)")
    List<Presentation> findPresentationsByCodeCisGroup(@Param("cisCode") Long cisCode);
}
