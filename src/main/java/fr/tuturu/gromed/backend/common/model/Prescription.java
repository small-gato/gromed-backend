package fr.tuturu.gromed.backend.common.model;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Prescription {
    @Id
    @Column(name = "nom_prescription", nullable = false)
    private String nomPrescription;
}
