package fr.tuturu.gromed.backend.common.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import fr.tuturu.gromed.backend.common.model.keys.ProduitCommandeKey;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;

@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProduitCommande implements Serializable {

    @EmbeddedId
    private ProduitCommandeKey key;

    @ManyToOne
    @MapsId("presentationId")
    @JoinColumn(name = "code_cip")
    private Presentation presentation;

    @ManyToOne
    @JsonIgnore
    @MapsId("commandeId")
    @JoinColumn(name = "identifiant_commande")
    private Commande commande;

    @Column(name="quantite_produit")
    private int quantite;


    public void replaceQuantite(int newQuantite) {
        if(newQuantite>=1){
            this.quantite = newQuantite;
        }
    }

    public void editQuantite(int quantite) {
        int newQuantite = this.quantite + quantite;
        if (newQuantite <= presentation.getStockLogique()) {
            this.quantite = newQuantite;
        }
    }
}
