package fr.tuturu.gromed.backend.common.repository;

import fr.tuturu.gromed.backend.common.model.ProduitCommande;
import fr.tuturu.gromed.backend.common.model.keys.ProduitCommandeKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProduitCommandeRepository extends JpaRepository<ProduitCommande, ProduitCommandeKey> {
}
