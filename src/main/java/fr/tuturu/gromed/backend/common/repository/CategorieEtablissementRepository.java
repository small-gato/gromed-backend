package fr.tuturu.gromed.backend.common.repository;

import fr.tuturu.gromed.backend.common.model.CategorieEtablissement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
public interface CategorieEtablissementRepository extends JpaRepository<CategorieEtablissement, Long> {
    @Transactional
    @Query("select c from CategorieEtablissement c where c.id=:idCategorie")
    Optional<CategorieEtablissement> findById(@Param("idCategorie") Long idCategorie);
}
