package fr.tuturu.gromed.backend.common.model;

import jakarta.persistence.*;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Getter
@Setter
public class GroupeGenerique {

    @Id
    @Column(name = "id_groupe", nullable = false)
    private Long idGroupe;

    @Column(name = "libelle", nullable = false)
    private String libelle;

    @OneToMany(mappedBy = "groupe", cascade = CascadeType.ALL)
    private Set<MedicamentAppartientGroupeGen> medicaments = new LinkedHashSet<>();

}
