package fr.tuturu.gromed.backend.common.exception;

import fr.tuturu.gromed.backend.auth.exception.BadRefreshTokenException;
import fr.tuturu.gromed.backend.auth.exception.EmptyRoleExceptions;
import fr.tuturu.gromed.backend.auth.exception.ExistingUserException;
import fr.tuturu.gromed.backend.auth.exception.RefreshTokenExpiredException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@Component
public class RestResponseExceptionHandler extends ResponseEntityExceptionHandler {
    
    //======================//
    // Base http exceptions //
    //======================//

    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    protected ResponseEntity<Object> handleNotFoundException(NotFoundException exception, WebRequest request) {
        return handleExceptionInternal(exception, exception.getErreurInfo(),
                HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected ResponseEntity<Object> handleBadRequestException(BadRequestException exception, WebRequest request) {
        return handleExceptionInternal(exception, exception.getErreurInfo(),
                HttpStatus.BAD_REQUEST, request);
    }



    @ExceptionHandler(ForbiddenException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    protected ResponseEntity<Object> handleForbiddenException(ForbiddenException exception, WebRequest request) {
        return handleExceptionInternal(exception, exception.getErreurInfo(),
                HttpStatus.FORBIDDEN, request);
    }

    @ExceptionHandler(InternalServerErrorException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    protected ResponseEntity<Object> handleInternalServerErrorException(InternalServerErrorException exception, WebRequest request) {
        return handleExceptionInternal(exception, exception.getErreurInfo(),
                HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    //=================//
    // AUTH EXCEPTIONS //
    //=================//
    @ExceptionHandler(BadRefreshTokenException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected ResponseEntity<Object> handleBadRefreshTokenException(BadRefreshTokenException exception, WebRequest request) {
        return handleExceptionInternal(exception, exception.getErreurInfo(),
                HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(EmptyRoleExceptions.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected ResponseEntity<Object> handleEmptyRoleExceptions(EmptyRoleExceptions exception, WebRequest request) {
        return handleExceptionInternal(exception, exception.getErreurInfo(),
                HttpStatus.BAD_REQUEST, request);
    }
    @ExceptionHandler(ExistingUserException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected ResponseEntity<Object> handleExistingUserException(ExistingUserException exception, WebRequest request) {
        return handleExceptionInternal(exception, exception.getErreurInfo(),
                HttpStatus.BAD_REQUEST, request);
    }
    @ExceptionHandler(RefreshTokenExpiredException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected ResponseEntity<Object> handleRefreshTokenExpiredException(RefreshTokenExpiredException exception, WebRequest request) {
        return handleExceptionInternal(exception, exception.getErreurInfo(),
                HttpStatus.BAD_REQUEST, request);
    }

    protected ResponseEntity<Object> handleExceptionInternal(Exception exception, ErreurInfo erreur,
                                                             HttpStatus status, WebRequest request) {
        RestError lError = new RestError(erreur.getMessage(),
                erreur.getCode(),
                status.value(),
                exception,
                request.getDescription(true));

        logger.error(lError.toString(), exception);
        return super.handleExceptionInternal(exception, lError,
                new HttpHeaders(), status, request);
    }
}