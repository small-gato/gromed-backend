package fr.tuturu.gromed.backend.common.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.LinkedHashSet;
import java.util.Set;

@Entity
@Getter
@Setter
public class Medicament {
    @Id
    @Column(name = "code_cis", nullable = false)
    private Long codeCIS;

    @Column(name = "nom_medicament")
    private String nomMedicament;

    @Column(name = "forme_pharmacetique")
    private String formePharmacetique;

    @Column(name = "etat_comercialisation")
    private String etatComercialisation;

    @Column(name = "lien_web")
    private String lienWeb;

    @Column(name = "fabricant")
    private String fabricant;

    @Column(name = "numero_asmr")
    private String numeroASMR;

    @Column(name = "libelle_asmr")
    private String libelleASMR;

    @OneToMany
    private Set<Prescription> prescriptions;

    @OneToMany(mappedBy = "medicament", cascade = CascadeType.ALL)
    private Set<MedicamentAppartientGroupeGen> groupes = new LinkedHashSet<>();

    @OneToMany(mappedBy = "medicament", cascade = CascadeType.ALL)
    private Set<MedicamentContientSubstance> substances;
}
