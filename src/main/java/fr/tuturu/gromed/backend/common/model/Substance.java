package fr.tuturu.gromed.backend.common.model;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Entity
@Getter
@Setter
public class Substance {

    @Id
    @Column(name = "code_substance", nullable = false)
    private Long codeSubstance;

    @Column(name = "nom_substance", nullable = false)
    private String nomSubstance;

    @OneToMany(mappedBy = "substance", cascade = CascadeType.ALL)
    private Set<MedicamentContientSubstance> medicaments;
}
